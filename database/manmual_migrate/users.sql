
INSERT INTO `users` (`name`, `username`, `email`, `email_verified_at`, `password`, `user_permission_id`, `remember_token`, `created_at`, `updated_at`) VALUES
	('Surachet Khamsunthorn', 'pop', 'pop@gmail.com', NULL, '$2y$10$NsuENmF1Aq3xjzRCtBcm1uLBoQ7QdK/I1z632n7eidaTZGqzWSHs.', 1, NULL, '2021-04-10 08:08:47', '2021-04-10 08:08:47'),
	('Christopher Whiteman', 'christopher', 'chris@gmail.com', NULL, '$2y$10$KOWPjBCEiXpfEutu/WW0pOyWjXXQYn.QmbPqQcnQUBKTRFNaBOnKq', 2, NULL, '2021-04-10 08:09:33', '2021-04-10 08:09:33'),
	('Logan Thunder', 'logan', 'logan@gmail.com', NULL, '$2y$10$ojLsN4Hp9K2GNHbZsiIkLurGiV2kxX9TC.OMQ68/sEQosN8V0Gmt6', 2, NULL, '2021-04-10 08:10:24', '2021-04-10 08:10:24'),
	('Antony Anderson', 'antony', 'antony@gmail.com', NULL, '$2y$10$GCAVKFOh4W42MniX6GjxLezXrlvx14w2LOEWMqy1fT0/tGrJOqxai', 2, NULL, '2021-04-10 08:11:00', '2021-04-10 08:11:00');
