
INSERT INTO `user_permission` (`up_desc`, `created_at`, `updated_at`) VALUES
	('Manager', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
	('Employee', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
