<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDecs extends Model
{
    protected $table = "product_desc";

    protected $primaryKey = "pd_id";

    protected $casts=[
    'pd_price'    =>  'integer',
    'sd_sale_amount'    =>  'integer'
];
}
