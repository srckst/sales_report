<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageFromCustomer extends Model
{
    protected $table = "message_from_customer";

    protected $primaryKey = "mfc_id";
}
