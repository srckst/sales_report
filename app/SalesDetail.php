<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesDetail extends Model
{
    protected $table = "sales_detail";

    protected $primaryKey = "sd_id";

    protected $casts=[
    'sd_sale_amount'    =>  'integer'
];
}
