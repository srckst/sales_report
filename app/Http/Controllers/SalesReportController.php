<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
use App\ProductDecs;
use App\SalesDetail;
use App\User;
use App\MessageFromCustomer;

class SalesReportController extends Controller
{
    public function __construct(){
        date_default_timezone_set("Asia/Bangkok");
    }
    public function index()
    {
        $user_permission = DB::table('user_permission')->where('up_id',Auth::user()->user_permission_id)->first();
        $total_user = User::where('user_permission_id',2)->count();

        $message = MessageFromCustomer::paginate(8);

        if (Auth::user()->user_permission_id == 1) {

            $title = 'Total Sale All';
            $title2 = 'Total Item Sales All';
            $table_title = 'Total Sale List';

            $total_sale_all = SalesDetail::select(DB::raw('sum(pd_price*sd_sale_amount) as pd_price'))->leftJoin('product_desc','pd_id','=','sd_pd_id')
            ->whereMonth('sd_date',Carbon::now()->month)
            ->value('pd_price');

            $total_item = SalesDetail::select(DB::raw('sum(sd_sale_amount) as sd_sale_amount'))->whereMonth('sd_date',Carbon::now()->month)
            ->value('sd_sale_amount');

            $sales = ProductDecs::selectRaw('SUM(pd_price*sd_sale_amount) as pd_price')
            ->leftJoin('sales_detail','pd_id','=','sd_pd_id')
            ->whereYear('sd_date',Carbon::now()->year)
            ->groupBy(DB::raw('Month(sd_date)'))
            ->pluck('pd_price');

            $item_list = ProductDecs::select('pd_name','sd_pd_id')
            ->leftJoin('sales_detail','pd_id','=','sd_pd_id')
            ->whereMonth('sd_date',Carbon::now()->month)
            ->groupBy(DB::raw('Month(sd_date),sd_pd_id'))
            ->pluck('pd_name');

            $amount = SalesDetail::select(DB::raw('sum(sd_sale_amount) as sd_sale_amount,sd_pd_id'))
            ->whereMonth('sd_date',Carbon::now()->month)
            ->groupBy(DB::raw('Month(sd_date),sd_pd_id'))
            ->pluck('sd_sale_amount');

            $month = SalesDetail::select(DB::raw('Month(sd_date) as month'))
            ->whereYear('sd_date',Carbon::now()->year)
            ->groupBy(DB::raw('Month(sd_date)'))
            ->pluck('month');

            $total_sale_list = SalesDetail::select(DB::raw('pd_name,SUM(sd_sale_amount) AS Total, SUM(pd_price) AS sale'))
            ->leftJoin('product_desc','pd_id','=','sd_pd_id')
            ->groupBy('pd_name')
            ->get();
        }
        else {

            $title = 'Employee Total Sale';
            $title2 = 'Employee Total Item Sales';
            $table_title = 'Employee Total Sale List';

            $total_sale_all = SalesDetail::select(DB::raw('sum(pd_price*sd_sale_amount) as pd_price'))
            ->leftJoin('product_desc','pd_id','=','sd_pd_id')
            ->where('user_id',Auth::user()->id)
            ->whereMonth('sd_date',Carbon::now()->month)
            ->value('pd_price');

            $total_item = SalesDetail::select(DB::raw('sum(sd_sale_amount) as sd_sale_amount'))
            ->leftJoin('users','id','user_id')
            ->where('user_id',Auth::user()->id)
            ->whereMonth('sd_date',Carbon::now()->month)
            ->value('sd_sale_amount');

            $sales = ProductDecs::selectRaw('SUM(pd_price*sd_sale_amount) as pd_price')
            ->leftJoin('sales_detail','pd_id','=','sd_pd_id')
            ->leftJoin('users','id','user_id')
            ->where('user_id',Auth::user()->id)
            ->whereYear('sd_date',Carbon::now()->year)
            ->groupBy(DB::raw('Month(sd_date)'))
            ->pluck('pd_price');

            $item_list = ProductDecs::select('pd_name','sd_pd_id')
            ->leftJoin('sales_detail','pd_id','=','sd_pd_id')
            ->where('user_id',Auth::user()->id)
            ->whereMonth('sd_date',Carbon::now()->month)
            ->groupBy(DB::raw('Month(sd_date),sd_pd_id'))
            ->pluck('pd_name');

            $amount = SalesDetail::select(DB::raw('sum(sd_sale_amount) as sd_sale_amount,sd_pd_id'))
            ->leftJoin('users','id','user_id')
            ->where('user_id',Auth::user()->id)
            ->whereMonth('sd_date',Carbon::now()->month)
            ->groupBy(DB::raw('Month(sd_date),sd_pd_id'))
            ->pluck('sd_sale_amount');

            $total_sale_list = SalesDetail::select(DB::raw('pd_name,SUM(sd_sale_amount) AS Total, SUM(pd_price) AS sale'))
            ->leftJoin('product_desc','pd_id','=','sd_pd_id')
            ->where('user_id',Auth::user()->id)
            ->groupBy('pd_name')
            ->get();
        }

        $view = view('home',[
            'total_sale_all'        =>      $total_sale_all,
            'total_item'            =>      $total_item,
            'user_permission'       =>      $user_permission,
            'title'                 =>      $title,
            'title2'                =>      $title2,
            'total_user'            =>      $total_user,
            'item_list'             =>      $item_list,
            'total_sale_list'       =>      $total_sale_list,
            'table_title'           =>      $table_title,
            'message'               =>      $message,
            'amount'                =>      $amount,
            'sales'                 =>      $sales,
        ]);

        return $view;
    }
}
