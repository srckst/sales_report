<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="RuangAdmin-master/img/logo/logo.png" rel="icon">
    <title>RuangAdmin - Register</title>
    <link href="RuangAdmin-master/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="RuangAdmin-master/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="RuangAdmin-master/css/ruang-admin.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-login">
    <!-- Register Content -->
    <div class="container-login">
        <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-7 col-md-4">
                <div class="card shadow-sm my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="login-form">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Register</h1>
                                    </div>
                                    <form method="POST" action="{{ route('register') }}">
                                        @csrf
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" id="exampleInputFirstName" value="{{ old('username') }}" placeholder="Enter Username" required autocomplete="username" autofocus>
                                                @error('username')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>User Permission</label>
                                                <select class="form-control @error('user_permission_id') is-invalid @enderror" name="user_permission_id" required autocomplete="user_permission_id">
                                                    <option value="" checked>Select Permission</option>
                                                    @foreach ($permission as $permissions)
                                                        <option value="{{ $permissions->up_id }}">{{ $permissions->up_desc }}</option>
                                                    @endforeach
                                                </select>
                                                @error('user_permission_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>Full Name</label>
                                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Enter Full Name" required autocomplete="name">
                                                    @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" aria-describedby="emailHelp"
                                                        placeholder="Enter Email Address" required autocomplete="email">
                                                        @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Password</label>
                                                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="new-password">
                                                            @error('password')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Repeat Password</label>
                                                            <input type="password" class="form-control" name="password_confirmation"
                                                            placeholder="Repeat Password" required autocomplete="new-password">
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary btn-block">Register</button>
                                                        </div>
                                                    </form>
                                                    <hr>
                                                    <div class="text-center">
                                                        <a class="font-weight-bold small" href="{{ route('login') }}">Already have an account?</a>
                                                    </div>
                                                    <div class="text-center">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Register Content -->
                    <script src="RuangAdmin-master/vendor/jquery/jquery.min.js"></script>
                    <script src="RuangAdmin-master/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <script src="RuangAdmin-master/vendor/jquery-easing/jquery.easing.min.js"></script>
                    <script src="RuangAdmin-master/js/ruang-admin.min.js"></script>
                </body>

                </html>
