> ## How to run project

> -    Install web server tools (Laragon is recommend)
> -    Install composer
> -    Install PHP (PHP 7.0+ recommend)
> -    Start service apache and MySQL
> -    Pull the project from bitbucket into web server root directory. (if using laragon , destination is C:\laragon\www)
> -    Create database name is `sales_report`
> -    Start command promt or terminal in project then run '`composer update`' 
> -    Open `.env.example` file then save as and rename to `.env`
> -    Run '`php artisan key:gen`' on command promt or terminal
> -    Open `.env` file then try follow this
> > *   `DB_CONNECTION=mysql`
> > *   `DB_HOST=127.0.0.1`
> > *   `DB_PORT=3306`
> > *   `DB_DATABASE=sales_report`
> > *   `DB_USERNAME=root`
> > *   `DB_PASSWORD=`
> -     Run '`php artisan migrate`' on command promt or terminal
> -     Go to manual_migrate folder (C:\laragon\www\sales_report\database\manmual_migrate) and see the SQL file then run SQL follow this sequence.
> > *   `user_permission.sql`
> > *   `users.sql`
> > *   `product_desc.sql`
> > *   `sales_detail.sql`
> > *   `message_from_customer.sql`
> -     Run '`php artisan serve`' on command promt or terminal and then go to http://localhost:8000 in your web browser
> -     Username and password follow this (Can sign up in login website)
> > *   username: pop  role:manager
> > *   username: christopher role:employee
> > *   username: logan role:employee
> > *   username: antony role:employee
> > *   all username password 123456789
